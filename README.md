# Throw fft at it

Junction 2021 challenge project. Visualizing space usage with fft.

Demo allows user to analyse movement frequencies in test data. Code can be run remotely or locally. 

Warning! Running large sets of data can crash older hardware!

We have a binder, check our project out there! [Binder](https://mybinder.org/v2/gl/Celppu%2Fthrow-fft-at-it/328d64e26dac7b710b4480d102173d9cb97d2180?urlpath=lab%2Ftree%2Fhelvar_main.ipynb)
